<?php

namespace Drupal\lwm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LwmAdminSettings implements Settings Form.
 *
 * @package Drupal\lwm\Form
 * @access public
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class LwmAdminSettings extends ConfigFormBase {

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'lwm_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lwm.settings',
    ];
  }

  /**
   * Build the LWM Config form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Getting the config default values.
    $config = $this->config('lwm.settings');

    // Building the Form.
    $form['lwm_about'] = [
      '#type' => 'item',
      '#markup' => $this->t('Add here the values of your Mailchimp lightweight connection.'),
    ];

    $form['lwm_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailchimp API KEY'),
      '#description' => $this->t('Fill the field with your Mailchimp API Key.'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['lwm_list_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailchimp List Name'),
      '#description' => $this->t('Fill the field with your selected Mailchimp List / Audience.'),
      '#default_value' => $config->get('list_name'),
    ];

    $form['lwm_tag_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailchimp Tag name'),
      '#description' => $this->t('Fill the field with your Mailchimp Tag Name (Not Segment).'),
      '#default_value' => $config->get('tag_name'),
    ];

    $form['lwm_form_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform ID'),
      '#description' => $this->t('Fill the field with your Webform ID (machine name).'),
      '#default_value' => $config->get('form_id'),
    ];

    $form['lwm_field_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field Mail of user'),
      '#description' => $this->t('Fill the field with your field name for mail (machine name).'),
      '#default_value' => $config->get('field_mail'),
    ];

    $form['lwm_field_firstname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field First Name of user'),
      '#description' => $this->t('Mailchimp requires firstname to add a new contact. Set the machine name.'),
      '#default_value' => $config->get('first_name'),
    ];

    $form['lwm_field_lastname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field Last Name of user'),
      '#description' => $this->t('Mailchimp requires lastname to add a new contact. Set the machine name.'),
      '#default_value' => $config->get('last_name'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Set the new values in the config object of the module.
    $this->config('lwm.settings')
      ->set('api_key', trim($form_state->getValue('lwm_api_key')))
      ->set('list_name', trim($form_state->getValue('lwm_list_name')))
      ->set('tag_name', trim($form_state->getValue('lwm_tag_name')))
      ->set('segment_name', trim($form_state->getValue('lwm_segment_name')))
      ->set('form_id', trim($form_state->getValue('lwm_form_id')))
      ->set('field_mail', trim($form_state->getValue('lwm_field_mail')))
      ->set('first_name', trim($form_state->getValue('lwm_field_firstname')))
      ->set('last_name', trim($form_state->getValue('lwm_field_lastname')))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
