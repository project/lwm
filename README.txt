LWM: The Lightweight Webform Mailchimp Module
---------------------------------------------

Manages a direct connection between a subscription form build with Webform or
with a custom implementation based in Form API, from your Drupal installation to
 a Mailchimp list / audience.
